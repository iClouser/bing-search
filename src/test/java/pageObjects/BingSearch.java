package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class BingSearch extends Base {

    // Class constructor
    public BingSearch(WebDriver driver) {
        super(driver);
        PageFactory.initElements(getWebDriver(), this);
    }

    @FindBy(id = "signinlinkhero")
    private WebElement signin;

    @FindBy(css = "ul#b_idProviders a")
    private WebElement signin_link;

    @FindBy(id = "id_l")
    private WebElement signin_toggle;

    By searchBoxLocator = By.id("sb_form_q");

    @FindBy(css = "input#sbBtn,label[for='sb_form_go']")
    WebElement searchButton;

    public boolean loginToBing(String email, String password) {
        boolean success;
        waitForElementToBeClickable(signin).click();
        Login loginPage = new Login(getWebDriver());
        success = loginPage.loginAsUser(email, password).loggedInSuccessfully();
        visit("http://bing.com");
        return success;

    }

    /**
     * Does a bing search based on the text provided.
     *
     * @param text
     */
    public void searchBing(String text) {
        WebElement searchbox = waitForElementToBeClickable(searchBoxLocator);
        searchbox.clear();
        type(text + Keys.ENTER, searchbox);
//        click(searchButton);
        waitForElementToBeClickable(searchBoxLocator);
    }

    public boolean searchedCorrectly(String text) throws InterruptedException {
        Thread.sleep(1000);
        String ftext = find(searchBoxLocator).getAttribute("value");
        return ftext.equals(text);
    }

}
