package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Generic Login class to login in to Microsoft's login pages.
 */
public class Login extends Base {

    @FindBy(css = "input[type='email']")
    private WebElement email_input;

    @FindBy(css = "input[value='Next']")
    private WebElement next_button;

    @FindBy(css = "input[type='password']")
    private WebElement password_input;

    @FindBy(css = "input[value='Sign in']")
    private WebElement signin_button;


    public Login(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    /**
     * This function assumes that you have made it to Microsofts login screen.
     *
     * @param email    - your email address
     * @param password your password
     */
    public Login loginAsUser(String email, String password) {
        System.out.println(getWebDriver().getTitle());
        type(email, waitForElementToBeClickable(email_input));
        waitForElementToBeClickable(next_button).click();
        type(password, waitForElementToBeClickable(password_input));
        waitForElementToBeClickable(signin_button).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean failedLogin = getWebDriver().findElements(By.cssSelector("div#passwordError")).size() > 0;
        System.out.println("Failed to login? " +failedLogin);
        Assert.assertFalse(failedLogin, "login error");
        System.out.println(getWebDriver().getTitle());
        return this;
    }

    public boolean loggedInSuccessfully() {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("home-index")));
        return getWebDriver().getTitle().equals("Microsoft account | Home");
    }
}
