package enums;

public enum Browser {
    CHROME,
    CHROMEMOBILE,
    FIREFOX,
    SAFARI,
    EDGE,
    IE
}
