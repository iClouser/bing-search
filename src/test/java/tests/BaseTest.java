package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.openqa.selenium.remote.BrowserType.*;

public class BaseTest {

    private Config conf;
    private static ThreadLocal<WebDriver> tlwd = new ThreadLocal<>();


    public WebDriver initDriver(Config conf) throws MalformedURLException {
        this.conf = conf;

        if (this.conf.isUseGrid()) initRemoteWebDriver();
        else {
            initLocalDriver();
        }

        tlwd.get().get(conf.getBaseUrl()); // go to initial url
        return tlwd.get();
    }

    private void initLocalDriver() {
        switch (conf.getBrowser()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/drivers/chromedriver.exe");
                ChromeOptions options = new ChromeOptions();

                options.addArguments(Arrays.asList("disable-popup-blocking", "start-maximized", "disable-infobars"));
                tlwd.set(new ChromeDriver(options));
                break;
            case CHROMEMOBILE:
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/drivers/chromedriver.exe");
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 5X");
                ChromeOptions mobile = new ChromeOptions();
                mobile.addArguments(Arrays.asList("disable-popup-blocking", "start-maximized", "disable-infobars"));
                mobile.setExperimentalOption("mobileEmulation", mobileEmulation);
                tlwd.set(new ChromeDriver(mobile));
                break;
            case EDGE:
                EdgeOptions edgeOptions = new EdgeOptions();
                tlwd.set(new EdgeDriver(edgeOptions));
                break;

        }
    }


    private void initRemoteWebDriver() throws MalformedURLException {
        switch (this.conf.getBrowser()) {
            case CHROME:
                ChromeOptions options = new ChromeOptions();
                options.addArguments(Arrays.asList("disable-popup-blocking", "start-maximized", "disable-infobars"));
                tlwd.set(new RemoteWebDriver(new URL(this.conf.getGridUrl() + "/wd/hub"), options));
                break;
            case CHROMEMOBILE:
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Nexus 5X");
                ChromeOptions mobile = new ChromeOptions();
                mobile.addArguments(Arrays.asList("disable-popup-blocking", "start-maximized", "disable-infobars"));
                mobile.setExperimentalOption("mobileEmulation", mobileEmulation);
                tlwd.set(new RemoteWebDriver(new URL(this.conf.getGridUrl() + "/wd/hub"), mobile));
                break;
        }
    }

    @AfterMethod
    protected void after() {
        if (tlwd.get() != null) {
            tlwd.get().quit();
        }
    }

    @AfterTest
    protected void afterTest() {
        if (tlwd.get() != null) {
            tlwd.get().quit();
        }
    }

    @AfterSuite
    protected void suiteQuit() {
        if (tlwd.get() != null) {
            tlwd.get().quit();
        }
    }

    public WebDriver getWebDriver() {
        return tlwd.get();
    }
}
