package tests;

import enums.Browser;

public class Config {

    String baseUrl = System.getProperty("baseUrl", "https://account.microsoft.com/");
    private Browser browser = Browser.valueOf(System.getProperty("browser", "EDGE").toUpperCase());
    private String email = System.getProperty("email", "NEEDANEMAIL@NOEMAIL.com");
    private String pwd = System.getProperty("pwd", "uNeedAPASS2017");

    private boolean useGrid = Boolean.parseBoolean(System.getProperty("useGrid", "false"));
    private String gridUrl = "http://0.0.0.0:4444";

    public Browser getBrowser() {
        return browser;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public boolean isUseGrid() {
        return useGrid;
    }

    public void setUseGrid(boolean useGrid) {
        this.useGrid = useGrid;
    }

    public String getGridUrl() {
        return gridUrl;
    }

    public void setGridUrl(String gridUrl) {
        this.gridUrl = gridUrl;
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


}
