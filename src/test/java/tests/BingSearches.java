package tests;

import com.google.gson.JsonParser;
import enums.Browser;
import org.testng.asserts.SoftAssert;
import pageObjects.BingSearch;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class BingSearches extends BaseTest {

    @Test
    public void bingSearchMobile() throws Throwable {
        Config cf = new Config();
        cf.setBrowser(Browser.CHROMEMOBILE); // Replace default browser to chrome.
        initDriver(cf);
        getWebDriver().get(cf.baseUrl);

        BingSearch bing = new BingSearch(getWebDriver());
        Assert.assertTrue(bing.loginToBing(cf.getEmail(), cf.getPwd()), "Something failed while logging in.");

        String word;
        SoftAssert sa = new SoftAssert();
        for (int i = 0; i < 21; i++) {
            Thread.sleep(1200);
           try {
                word = getRandomWord();
                bing.searchBing(word);
                if (i > 1) {
                    sa.assertTrue(bing.searchedCorrectly(word));
                }
            }
            catch(Exception e){
                System.out.println("Failed to get random word.");
            }
        }
        sa.assertAll();
        System.out.println("All Done!");
    }

    @Test
    public void bingDesktop() throws Throwable {
        Config cf = new Config();
        cf.setBrowser(Browser.CHROME); // Replace default browser to chrome.
        initDriver(cf);
        getWebDriver().get(cf.baseUrl);

        BingSearch bing = new BingSearch(getWebDriver());
        Assert.assertTrue(bing.loginToBing(cf.getEmail(), cf.getPwd()), "Something failed while logging in.");

        String word;
        SoftAssert sa = new SoftAssert();
        for (int i = 0; i < 31; i++) {
            Thread.sleep(1200);
           try {
               word = getRandomWord();
               bing.searchBing(word);
               if (i > 1) {
                   sa.assertTrue(bing.searchedCorrectly(word));
               }
           }
           catch (Exception e){
               System.out.println("Failed to get random word.");
           }
        }
        sa.assertAll();
        System.out.println("All Done!");
    }

    @Test(enabled = false)
    public void bingSearchEdge() throws Throwable {
        String word;

        Config cf = new Config();
        cf.setUseGrid(false);
        initDriver(cf);
        getWebDriver().get(cf.baseUrl);

        BingSearch bing = new BingSearch(getWebDriver());
        Assert.assertTrue(bing.loginToBing(cf.getEmail(), cf.getPwd()), "Something failed while logging in.");
        SoftAssert sa = new SoftAssert();
        for (int i = 0; i < 5; i++) {
            Thread.sleep(1200);
            word = getRandomWord();
            bing.searchBing(word);
            if (i > 1) {
                sa.assertTrue(bing.searchedCorrectly(word));
            }
//            System.out.println(word);
        }
        sa.assertAll();
        System.out.println("All Done!");
    }

    /**
     * Random word api I found on the internet
     *
     * @return a single word
     */
    private String getRandomWord() throws Exception {
        URL url = new URL("https://namey.muffinlabs.com/name.json?count=1&with_surname=true&frequency=all");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        return new JsonParser().parse(content.toString()).getAsJsonArray().get(0).toString().replaceAll("\"", "");
    }
}
