# bing-search
Project that does searches with Bing. It also has to the ability to login in before searching with bing

## Setup 
1. Download or clone repository
2. Open bing-search/src/test/java/tests/Config.java
3. Replace email place holder with your email 
4. Replace pwd with your password 
5. Run in your ide of choice or see alternate option below (6).
6. Open a command prompt in bing-search and run the command gradlew build
7. Profit!


#### Note on Driver Executables
ChromeDriver download: [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads "ChromeDriver")


